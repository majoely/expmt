"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/projects/views.py

"""
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from datetime import datetime
#HttpResponseRedirect
#from django.core.urlresolvers import reverse
#import datetime

from projects.models import Project, Pcomment


# Create your views here.
def index(request):
    project_list = Project.objects.all().order_by('-pub_date')
    context = {'project_list': project_list}
    return render(request, 'projects/index.html', context)


def project(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    return render(request, 'projects/project.html', {'project': project})


def edit(request, project_id):
    try:
        project = Project.objects.get(pk=project_id)
    except Project.DoesNotExist:
        project = Project(name="project", description="agile project",
            pub_date=datetime.now(), up_date=datetime.now(), author="none",
            contact_name="none", contact_email="none")
        project.save()
    return render(request, 'projects/edit.html', {'project': project})
    #reply = "Edit page has not yet been implemented<br />"
    #reply += "project_id: %s" % project_id
    #return HttpResponse(reply)


def post(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    project.name = request.POST['name']
    project.author = request.POST['author']
    project.contact_name = request.POST['cname']
    project.contact_email = request.POST['cemail']
    project.description = request.POST['description']
    project.up_date = datetime.now()
    check = False
    if 'complete' in request.POST:
        check = True
    project.complete = check
    project.save()
    return HttpResponseRedirect(reverse('projects:project', args=(project_id,)))
    #reply = "Post page has not yet been implemented<br />"
    #reply += "project_id: %s" % project_id
    #return HttpResponse(reply)


def delete(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    project.delete()
    return HttpResponseRedirect(reverse('projects:index'))
    #reply = "Delete page has not yet been implemented<br />"
    #reply += "project_id: %s" % project_id
    #return HttpResponse(reply)


def comment(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    post_author = request.POST['author']
    post_comment = request.POST['comment']
    cur_date = datetime.now()
    com = Pcomment(project=project, comment=post_comment, author=post_author,
        pub_date=cur_date)
    com.save()
    return HttpResponseRedirect(reverse('projects:project', args=(project_id,)))
    reply = "Comment page has not yet been implemented<br />"
    reply += "project_id: %s" % project_id
    return HttpResponse(reply)
