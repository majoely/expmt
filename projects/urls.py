"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/projects/urls.py

"""

from django.conf.urls import patterns, url

from projects import views

urlpatterns = patterns('',
    #ex: /projects/
    url(r'^$', views.index, name='index'),
    #ex: /projects/3/
    url(r'^(?P<project_id>\d+)/$', views.project, name='project'),
    #ex: /projects/3/edit
    url(r'^(?P<project_id>\d+)/edit/$', views.edit, name='edit'),
    #ex: /projects/3/post
    url(r'^(?P<project_id>\d+)/post/$', views.post, name='post'),
    #ex: /projects/3/delete
    url(r'^(?P<project_id>\d+)/delete/$', views.delete, name='delete'),
    #ex: /projects/3/comment
    url(r'^(?P<project_id>\d+)/comment/$', views.comment, name='comment'),
)
