"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/projects/models.py

"""

from django.db import models


# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=600)
    pub_date = models.DateTimeField()
    up_date = models.DateTimeField()
    complete = models.BooleanField(default=False)
    published = models.BooleanField(default=True)
    author = models.CharField(max_length=100)
    contact_name = models.CharField(max_length=100)
    contact_email = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Pcomment(models.Model):
    project = models.ForeignKey(Project)
    comment = models.CharField(max_length=600)
    pub_date = models.DateField()
    author = models.CharField(max_length=100)

    def __str__(self):
        return self.comment
