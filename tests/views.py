"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/tests/views.py

"""

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
#from django.http import HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime

from tests.models import Test, Comment
from story.models import Card


# Create your views here.
def test(request, test_id):
    test = get_object_or_404(Test, pk=test_id)
    return render(request, 'tests/test.html', {'test': test})
    #reply = "Test page has not yet been implemented<br />"
    #reply += "test_id: %s" % test_id
    #return HttpResponse(reply)


def edit(request, test_id):
    try:
        test = Test.objects.get(pk=test_id)
    except Test.DoesNotExist:
        f_card = get_object_or_404(Card, pk=request.POST['cardid'])
        test = Test(card=f_card, acceptance="acceptance for test",
            pub_date=datetime.now(), author="author")
        test.save()
    return render(request, 'tests/edit.html', {'test': test})
    #reply = "Edit page has not yet been implemented<br />"
    #reply += "test_id: %s" % test_id
    #return HttpResponse(reply)


def post(request, test_id):
    test = get_object_or_404(Test, pk=test_id)
    test.acceptance = request.POST['acceptance']
    test.author = request.POST['author']
    check = False
    if 'accepted' in request.POST:
        check = True
    test.accepted = check
    test.save()
    card = test.card_id
    return HttpResponseRedirect(reverse('story:story', args=(card,)))
    #reply = "Post page has not yet been implemented<br />"
    #reply += "test_id: %s" % test_id
    #return HttpResponse(reply)


def delete(request, test_id):
    test = get_object_or_404(Test, pk=test_id)
    card = test.card_id
    test.delete()
    return HttpResponseRedirect(reverse('story:story', args=(card,)))
    #reply = "Delete page has not yet been implemented<br />"
    #reply += "test_id: %s" % test_id
    #return HttpResponse(reply)


def comment(request, test_id):
    f_test = get_object_or_404(Test, pk=test_id)
    post_comment = request.POST['comment']
    post_author = request.POST['author']
    date = datetime.now()
    comment = Comment(test=f_test, comment=post_comment, author=post_author,
        pub_date=date)
    comment.save()
    return HttpResponseRedirect(reverse('tests:test', args=(test_id,)))
    #reply = "Comment page has not yet been implemented<br />"
    #reply += "test_id: %s" % test_id
    #return HttpResponse(reply)