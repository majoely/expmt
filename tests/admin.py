from django.contrib import admin
from tests.models import Test, Comment


# Register your models here.
admin.site.register(Test)
admin.site.register(Comment)