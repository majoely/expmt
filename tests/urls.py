"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/tests/urls.py

"""

from django.conf.urls import patterns, url
from tests import views


urlpatterns = patterns('',
    #ex: /tests/3/
    url(r'^(?P<test_id>\d+)/$', views.test, name='test'),
    #ex: /tests/3/edit
    url(r'^(?P<test_id>\d+)/edit/$', views.edit, name='edit'),
    #ex: /tests/3/post
    url(r'^(?P<test_id>\d+)/post/$', views.post, name='post'),
    #ex: /tests/3/delete
    url(r'^(?P<test_id>\d+)/delete/$', views.delete, name='delete'),
    #ex: /tests/3/comment
    url(r'^(?P<test_id>\d+)/comment/$', views.comment, name='comment'),
)