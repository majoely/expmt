"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/story/urls.py

"""

from django.conf.urls import patterns, url

from story import views

urlpatterns = patterns('',
    #ex: /story/3/
    url(r'^(?P<story_id>\d+)/$', views.story, name='story'),
    #ex: /story/3/edit
    url(r'^(?P<story_id>\d+)/edit/$', views.edit, name='edit'),
    #ex: /story/3/post
    url(r'^(?P<story_id>\d+)/post/$', views.post, name='post'),
    #ex: /story/3/delete
    url(r'^(?P<story_id>\d+)/delete/$', views.delete, name='delete'),
    #ex: /story/3/comment
    url(r'^(?P<story_id>\d+)/comment/$', views.comment, name='comment'),
)