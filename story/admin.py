from django.contrib import admin
from story.models import Card, Comment


# Register your models here.
admin.site.register(Card)
admin.site.register(Comment)