"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/story/models.py

"""

from django.db import models
from projects.models import Project


# model to represent user stories
# cards are related to projects
# A project will have many cards
class Card(models.Model):
    project = models.ForeignKey(Project)
    story = models.CharField(max_length=600)
    points = models.IntegerField()
    pub_date = models.DateTimeField()
    up_date = models.DateTimeField()
    complete = models.BooleanField(default=False)
    published = models.BooleanField(default=True)
    author = models.CharField(max_length=100)

    def __str__(self):
        return self.story


# model to represent comments on Cards
# a card will have many comments
class Comment(models.Model):
    card = models.ForeignKey(Card)
    comment = models.CharField(max_length=600)
    hours = models.IntegerField(default=0)
    author = models.CharField(max_length=100)
    pub_date = models.DateTimeField()

    def __str__(self):
        return self.comment