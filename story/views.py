"""

   Copyright 2014 Joel Compton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   ./exmpt/story/views.py

"""

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from datetime import datetime

from story.models import Card, Comment
from projects.models import Project


# Create your views here.
def story(request, story_id):
    card = get_object_or_404(Card, pk=story_id)
    return render(request, 'story/story.html', {'card': card})
    #reply = "Story page has not yet been implemented<br />"
    #reply += "story_id: %s" % story_id
    #return HttpResponse(reply)


def edit(request, story_id):
    try:
        card = Card.objects.get(pk=story_id)
    except Card.DoesNotExist:
        proj = get_object_or_404(Project, pk=request.POST['projectid'])
        card = Card(project=proj, story="User Story", points=0,
            pub_date=datetime.now(), up_date=datetime.now(), author="author")
        card.save()
    return render(request, 'story/edit.html', {'card': card})
    #reply = "Edit page has not yet been implemented<br />"
    #reply += "story_id: %s" % story_id
    #return HttpResponse(reply)


def post(request, story_id):
    card = get_object_or_404(Card, pk=story_id)
    card.story = request.POST['story']
    card.points = int(request.POST['points'])
    card.author = request.POST['author']
    card.up_date = datetime.now()
    check = False
    if 'complete' in request.POST:
        check = True
    card.complete = check
    card.save()
    project = card.project_id
    return HttpResponseRedirect(reverse('projects:project', args=(project,)))
    #reply = "Post page has not yet been implemented<br />"
    #reply += "story_id: %s" % story_id
    #return HttpResponse(reply)


def delete(request, story_id):
    card = get_object_or_404(Card, pk=story_id)
    project = card.project_id
    card.delete()
    return HttpResponseRedirect(reverse('projects:project', args=(project,)))
    #reply = "Delete page has not yet been implemented<br />"
    #reply += "story_id: %s" % story_id
    #return HttpResponse(reply)


def comment(request, story_id):
    story_card = get_object_or_404(Card, pk=story_id)
    post_author = request.POST['author']
    post_hours = request.POST['hours']
    post_comment = request.POST['comment']
    cur_date = datetime.now()
    com = Comment(card=story_card, comment=post_comment, hours=post_hours,
        author=post_author, pub_date=cur_date)
    com.save()
    return HttpResponseRedirect(reverse('story:story', args=(story_id,)))
    #reply = "Comment page has not yet been implemented<br />"
    #reply += "story_id: %s" % story_id
    #return HttpResponse(reply)