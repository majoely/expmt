# README #

### What is this repository for? ###

* This is for an internal project management tool for Exium
* Version 0.2

### What to expect ###

* Viewing project details and the associated story cards
* Adding users stories to projects
* Editing user stories
* Deleting user stories

### How do I get set up? ###

* Pull the repository using git
* Dependencies 
    Python 3 (I'm running 3.4 but all python3 should run)
    Django
    SQLite
* Database configuration
    No database configuration should be necessary as a database is included (for testing only)
    If you wish you can recreate the database by removing the db.sqlite3 file from the project root directory and run "python manage.py syncdb" in the root directory and then you will have to add entries through the command line or the admin app (details below)
* How to the project
    From the root directory run "python manage.py runserver"
    Then go to "http://127.0.0.1:8000"
    You should get a "Welcome to Python Django message"
    To run the app go to "http://127.0.0.1:8000/projects/" and view my noob attempt at python django
    To add more entries to the db go to "http://127.0.0.1:8000/admin/"
        user - joel
        pass - password1
* Deployment instructions

### Contribution guidelines ###

Write and push, all entries appreiciated
Any questions email me.

### Who do I talk to? ###

joel@psgwebsites.co.nz
If you want to learn python and django go to:
http://www.codecademy.com/tracks/python
https://docs.djangoproject.com/en/1.6/intro/tutorial01/